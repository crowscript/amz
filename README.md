# Project Title 
---
One Paragraph of project description goes here

## Table of Contents
---
- [About the Project](#about-the-project)
- [Status](#status)
- [Screenshots](#screenshots)
- [Live Demo](#live-demo)
- [Directory Structure](#directory-structure)
- [Getting Started](#getting-started)
- [Features](#features)
	- [Biggest Features 1](#biggest-features-1)
	- [Biggest Features 2](#biggest-features-2)
	- [Additional Features](#additional-features)
- [Built With](#built-with)
	- [How to build](#how-to-build)
	- [Requirements](#requirements)
		- [How install Requirements](#how-install-requirements)
	- [Dependencies](#dependencies)
		- [How install Dependencies](#how-install-the-dependencies)
- [How to use](#how-to-use)
	- [Installing](#installing)
	- [API Reference](#api-reference)
	- [Running the tests](#running-the-tests)
	- [Deployment](#deployment)
- [Contribute](#contribute)
	- [Reporting Bugs](#reporting-bugs)
- [Release Process](#release-process)
	- [Versioning](#versioning)
	- [Changelog](#changelog)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## About the Project
---
Here you can provide more details about the project
- What is your project?
- What is the intended use of your project?
- How does it work?
- Who uses it?
- How can others benefit from using your project?
- What features does your project provide?
- Short motivation for the project? (Don't be too long winded)
- Links to the project site

```
Show some example code to describe what your project does
Show some of your APIs
```

## Status
---
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/atlassian/adf-builder-javascript.svg) ![npm](https://img.shields.io/npm/v/npm.svg) ![node](https://img.shields.io/node/v/passport.svg) ![GitHub release](https://img.shields.io/github/release/qubyte/rubidium.svg) ![PHP version from PHP-Eye](https://img.shields.io/php-eye/symfony/symfony.svg) ![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)
[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/uses-css.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/powered-by-netflix.svg)](http://forthebadge.com)

Describe the current release and any notes about the current state of the project. Examples: currently compiles on your host machine, but is not cross-compiling for ARM, APIs are not set, feature not implemented, etc.

[Make own badge](https://shields.io/#your-badge)

## Screenshots
---
Include logo/demo screenshot etc.

![Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/HTML5_logo_resized.svg/170px-HTML5_logo_resized.svg.png)
![Screen](https://lh3.googleusercontent.com/oQ5DNeAAoo3DNecibTFqZXjzVbr_2TU_QOJ7-tIx9RByCcAafggfmREzAjjL1_djQjRGdLJusQ=w640-h400-e365)

## Live Demo
---
A live demo is always the best way to show off your plugin. You can link off to a snapshot or live demo hosted on your Grafana.

[sites.google.com](hhttps://sites.google.com/view/crowscript)

## Directory Structure
---
```
┌── .gitignore
├── .htaccess
├── .sass-lint.yml
├── .travis.yml
├── src
│   ├── browserconfig.xml
│   ├── crossdomain.xml
│   ├── humans.txt
│   ├── icons
│   │   ├── apple-touch-icon-114x114-precomposed.png
│   │   ├── apple-touch-icon-57x57-precomposed.png
│   │   ├── apple-touch-icon-72x72-precomposed.png
│   │   ├── apple-touch-icon-precomposed.png
│   │   ├── apple-touch-icon.png
│   │   ├── favicon.ico
│   │   └── favicon.png
│   ├── img
│   ├── index.html
│   ├── js
│   ├── robots.txt
│   └── scss
│       ├── atoms
│       │   └── _index.scss
│       ├── base
│       │   ├── _base.scss
│       │   └── _index.scss
│       ├── layout
│       │   └── _index.scss
│       ├── libs
│       │   ├── _index.scss
│       │   ├── _normalize.scss
│       │   └── _pesticide.scss
│       ├── molecules
│       │   └── _index.scss
│       ├── organisms
│       │   └── _index.scss
│       ├── overrides
│       │   └── _index.scss
│       ├── states
│       │   ├── _index.scss
│       │   └── _print.scss
│       ├── themes
│       │   └── rebeccapurple.scss
│       ├── utilities
│       │   ├── _colors.scss
│       │   ├── _config.scss
│       │   ├── _fonts.scss
│       │   ├── _functions.scss
│       │   ├── _index.scss
│       │   ├── _mixins.scss
│       │   └── _typography.scss
│       ├── styles.scss
│       └── _shame.scss
├── gulpfile.js
└── package.json
```

## Getting Started
---
This section should provide instructions for other developers to

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

1. If needed, [install](http://blog.nodeknockout.com/post/65463770933/how-to-install-node-js-and-npm) `node` and `npm` (Node Package Manager).
- If needed, install `gulp` with `npm install gulp -g`.
- Clone this repo with `git clone https://github.com/minamarkham/sassy-starter` or download the zip.
- In terminal, `cd` to the folder containing your project. Alternatively, you can type `cd ` and drag the location of the folder into your terminal and hit enter (on Macs).
- In terminal, type `npm install`. If (and _only_ if) `npm install` isn't working, try `sudo npm install`. This should install all [dependencies](#dependencies).
- In terminal, enter `gulp`.
- Your browser should open at `http://localhost:3000`. You can access this same page on any device on the same wifi network and they'll see whats on your screen. It'll even sync scrolls and clicks!
- Edit your code inside of the `src` folder.
- Your complied and minified css, html, and javascript files will be created and updated in `dist/`. Never edit files within the `dist/` folder, as it gets deleted frequently.
- Keep `gulp` running while you're making changes. When you want to stop the gulp task, hit `ctrl + C`.

_For theming: add separate file (theme.scss) in`src/scss/themes/`, override the default `$theme` variable, and run `gulp themes`._

## Features
---
What makes your project stand out?

### Biggest Features 1
The larger features can demand their own section.

`You can add code blocks.`

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

![featurs1](https://grafana.com/assets/img/blog/mixed_styles.png)

### Biggest Features 2
The larger features can demand their own section.
	
### Additional Features
For non-headline features, bulleted lists can be effective and concise way to increase skimmability.

- Live reloading with BrowserSync
- Image Minification
- Github Pages deployment
- Sass linting (based on [default](https://github.com/sasstools/sass-lint/blob/master/lib/config/sass-lint.yml) config)
- Autoprefixer configuration
- SMACSS and Atomic Design-based folder structure
- `px` to `em`, `px` to `rem` and other useful functions.
- Mixins for inlining media queries.
* Useful CSS helper classes.
* Default print styles, performance optimized.
* "Delete-key friendly." Easy to strip out parts you don't need.
- Includes:
  - [`Normalize.css`](https://necolas.github.com/normalize.css/)
    for CSS normalizations and common bug fixes
  - [`CSS Pesticide`](https://pesticide.io)
    for easy CSS debugging
  - [`jQuery`](https://jquery.com/) via CDN, with a local fallback
  - [`Modernizr`](http://modernizr.com/), via CDN, for feature
    detection
  - [`Apache Server Configs`](https://github.com/h5bp/server-configs-apache)
    that, among other, improve the web site's performance and security
		
	
## Built With
---
* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

### How to Build
How to build your project

### Requirements
What things you need to install the software and how to install them

- Node/NPM
- LibSass
- Gulp

#### How install Requirements
How to install the requirements.

### Dependencies
Dependencies that need to be installed for building/using your project
```
  "browser-sync": "^2.0.0-rc4",
  "colors": "^1.1.2",
  "del": "^2.0.2",
  "gulp-autoprefixer": "^2.1.0",
  "gulp-concat": "^2.4.3",
  "gulp-gh-pages": "^0.4.0",
  "gulp-imagemin": "^2.1.0",
  "gulp-jshint": "^1.9.0",
  "gulp-minify-css": "^0.3.12",
  "gulp-minify-html": "^0.1.8",
  "gulp-notify": "^2.2.0",
  "gulp-plumber": "^0.6.6",
  "gulp-rename": "^1.2.0",
  "gulp-sass": "^1.3.2",
  "gulp-sass-lint": "1.0.1",
  "gulp-size": "^1.2.0",
  "gulp-sourcemaps": "^1.5.2",
  "gulp-uglify": "^1.0.2",
  "imagemin-pngquant": "^4.0.0",
  "sassdoc": "^2.1.15",
  "vinyl-paths": "^2.0.0"
```
#### How install Dependencies
Instructions for installing the dependencies

## How to Use
---
If people like your project they’ll want to learn how they can use it. To do so include step by step guide to use your project.

### Installing
Provide step by step series of examples and explanations about how to get a development env running.

Say what the step will be

Include a link to your github reposistory (you have no idea how people will findy our code), and also a summary of how to clone.

This project is [hosted on GitHub](https://github.com/embeddedartistry/embedded-resources). You can clone this project directly using this command:

```
git clone git@github.com:embeddedartistry/embedded-resources.git
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

### API Reference
Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. 
For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

### Running the Tests
Describe how to run unit tests for your project.
```
Examples should be included
```

### Deployment
Add additional notes about how to deploy this on a live system

## Contribute
---
Let people know how they can contribute into your project. A [contributing guideline](https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md) will be a big plus.
Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

### Reporting Bugs
Please list all bugs and feature requests in the Github issue tracker.
This section guides you through submitting a bug report for Atom. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

Before creating bug reports, please check this list as you might find out that you don't need to create one. When you are creating a bug report, please include as many details as possible. Fill out the required template, the information it asks for helps us resolve issues faster.

## Release Process
---
Talk about the release process. How are releases made? What cadence? How to get new releases?

### Versioning
This project uses [Semantic Versioning](http://semver.org/). For a list of available versions, see the [repository tag list](https://github.com/your/project/tags).

### Changelog
The README can also include a trailing list of changes for recent versions.

**v1.0.1**
	- Updated the knickerbocker widget to accept multi-input threading.
	- Dialed up the down.
	- Righted the left
**v1.0.0**
	- Fixed bugs introduced in 1.0.4.
	- Introduced bugs that will be fixed in 1.0.6

## Authors
---
* **Stanislav Vranic** - *Developing* - [@crowscript](http://crowscript.com)
* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License
---
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
[Choose the License](https://choosealicense.com/)

## Acknowledgments
---
This toolkit is based on the work of the following fine people & projects.

- [HTML5 Boilerplate](https://github.com/h5bp/html5-boilerplate)
- [Scalable and Modular Architecture for CSS](http://smacss.com/book) (<abbr title="Scalable and Modular Architecture for CSS">SMACSS</abbr>)
- [Atomic Design](http://atomicdesign.bradfrost.com)

Provide proper credits, shoutouts, and honorable mentions here. Also provide links to relevant repositories, blog posts, or contributors worth mentioning.

Give proper credits. This could be a link to any repo which inspired you to build this project, any blogposts or links to people who contributed in this project. If you used external code, link to the original source.

**[Back to top](#table-of-contents)**
